#include <stdio.h>
#include <sys/time.h>
#include <time.h>
#include "rtime.h"


int main(void)
{
time_t current_time = time(NULL);
struct tm* cur_time = { 0 };
time_t time_tmp	= 0;

/*    cur_time->tm_year+1900,
    cur_time->tm_mon+1,
    cur_time->tm_mday,
    cur_time->tm_hour,
    cur_time->tm_min,
    cur_time->tm_sec,
*/

    
    cur_time = localtime(&current_time);
    
    printf("localtime returns %d\n",current_time);


    printf("mktime returns %d\n",mktime(cur_time)); 

    printf("%d\n",current_time);

    printf("\n Epoch from %d-%d-%d %d:%d:%d = %d\n" ,
		    cur_time->tm_year+1900,
		    cur_time->tm_mon+1,
		    cur_time->tm_mday,
	    	    cur_time->tm_hour,
		    cur_time->tm_min,
		    cur_time->tm_sec,
		    current_time);

    cur_time->tm_hour = 0;
    cur_time->tm_min = 0;
    cur_time->tm_sec = 0;
    cur_time->tm_year = 104;
    cur_time->tm_mon = 0;
    cur_time->tm_mday = 1;
    cur_time->tm_wday = 0;
    cur_time->tm_yday = 0;
    cur_time->tm_isdst = -1;
    cur_time->tm_gmtoff = 0;

    printf("mktime 2004 returns %d\n",mktime(cur_time)); 
    
    time_tmp = 1072904400;

    cur_time = localtime(&time_tmp);

    printf("\n Epoch from %d-%d-%d %d:%d:%d = %d\n" ,
		    cur_time->tm_year+1900,
		    cur_time->tm_mon+1,
		    cur_time->tm_mday,
	    	    cur_time->tm_hour,
		    cur_time->tm_min,
		    cur_time->tm_sec,
		    time_tmp);
    
    cur_time->tm_hour = 0;
    cur_time->tm_min = 0;
    cur_time->tm_sec = 0;
    cur_time->tm_year = 104;
    cur_time->tm_mon = 0;
    cur_time->tm_mday = 0;
    cur_time->tm_wday = 0;
    cur_time->tm_yday = 10;
    cur_time->tm_isdst = -1;
    cur_time->tm_gmtoff = 0;

#define DAY_SEC 86400   
    
    time_tmp = 10 * DAY_SEC;

    printf("mktime PG 2004 returns %d\n",time_tmp);

    cur_time = localtime(&time_tmp);

    printf("PG 2004 asctime %s\n gmtoff = %d\n",asctime(cur_time),cur_time->tm_gmtoff);

    time_tmp = 0;
    cur_time = localtime(&time_tmp);

    printf("90000 %s\n gmtoff = %d",asctime(cur_time),cur_time->tm_gmtoff);

};
