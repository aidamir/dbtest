/*******************************************************************
*
*    DESCRIPTION: logging
*
*    AUTHOR: Amosov Evgeniy, mastak.ru
*
*    HISTORY:
*
*    DATE:11/22/2002
*
*******************************************************************/

/** include files **/
#include <stdarg.h>
#include <stdio.h>
#include "dbtest.h"


/** private data **/
static char log_buffer[MAX_LOG_MESSAGE];
static char mark_buf[200];

int bs_log(int level, char * file, int line, char * function, char *fmt, ...){

	int n=0;
	int nn=0;
	va_list ap;
	

	if ( level<= BS_LOG_LEVEL ) {


	   nn = snprintf(mark_buf,200, "level_%d %s:%d:%s():", level, file, line, function);
       
       va_start(ap, fmt);
	   n=vsnprintf( log_buffer, MAX_LOG_MESSAGE, fmt, ap);
	   va_end(ap);
	    
       n=fprintf(stderr,"%s %s",mark_buf, log_buffer);
       return n;
	}

	return 0;

}


