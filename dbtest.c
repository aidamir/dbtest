#include "pg_macro.h"
#include "util.h"
#include <stdio.h>
#include "c.h"
#include <stdarg.h>
#include <stdint.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdint.h>
#include <unistd.h>
#include <libpq-fe.h>

#include <sys/socket.h>
#include <sys/poll.h>
#include <errno.h>
#include "dbtest.h"
#include <time.h>

//#define BE.max_conn 32

#define PQ_SOCK_CLEAN while ( res = PQgetResult(conn) ) PQclear(res);

#define PQ_SEND_QUERY(q, p_name)\
    if ( !PQsendQuery(conn,q) )\
       bs_log(1,BS_LOG_MARK,"%s:Query dispatch failed!\n",p_name);

#define PQ_RES_COK(msg)\
    if (PQresultStatus(res) != PGRES_COMMAND_OK)\
    {    \
    bs_log(1,BS_LOG_MARK,"%s!\n\t%s",msg,PQerrorMessage(conn));\
    PQclear(res);\
    return 0;\
    }

#define PQ_RES_TOK(msg)\
    if (PQresultStatus(res) != PGRES_TUPLES_OK)\
    {    \
    bs_log(1,BS_LOG_MARK,"%s!\n\t%s",msg,PQerrorMessage(conn));\
    PQclear(res);\
    return 0;\
    }


#define GET_MICROSEC(tvn) ( ((tvn)->tv_sec) * 1000000 + (tvn)->tv_usec )
#define PUT_MICROSEC(tvn, microsec) { ((tvn)->tv_sec) = ((microsec)/1000000); \
                     ((tvn)->tv_usec) = ( (microsec) - ( ((tvn)->tv_sec) * 1000000 ) ); }

#define GET_MILLISEC(tvn) ( ((tvn)->tv_sec) * 1000 + ( ((tvn)->tv_usec)/1000 ) * ((tvn)->tv_usec) )

PGconn *conni[1000] = {0};

struct pollfd pfd[1000 + 1] = {0}; // +1 command pipe

extern v_def v_defs[32];
extern q_def q_defs[32];

extern v_def **vdefs;
extern q_def **qdefs;

extern int vdef_cnt;
extern int qdef_cnt;
extern struct backend_t BE;

q_stat *qstat;

char qbuf[8192] = {""};
char numstr[128] = {""};

int processed = 0;
int qprocessed = 0;
int sent = 0;
int sheduler_called = 0;
int r = 0;
int limit = 0;
int stolen_conn_count = 0;
int report_interval = 0;
int last_report_time = 0;
int last_stat_reset_time = 0;

void q_upstat(q_stat *qs);

// void call_timer();
void log_report();

int q_send(q_def *q);

FILE *FERR;

struct timeval *time_diff(struct timeval *tvo,
                          struct timeval *tvn,
                          struct timeval *diff);


struct timeval test_start_time = {0};


int qnum = 0;

PGconn *get_stolen_pgconn() {
    int i;
    PGconn *tmp;
    vseravnoishi:

    for (i = stolen_conn_count; (i < BE.max_conn) && PQisBusy(conni[i]); i++);

    tmp = conni[i];

    conni[i] = conni[stolen_conn_count];

    conni[stolen_conn_count++] = tmp;

    if (i < BE.max_conn)
        return conni[i];
    else {
        printf("Stolen free pgconn not found...\n");
        return NULL;
    }
//	goto vseravnoishi;
}

PGconn *get_free_pgconn() {
    int i;
// vseravnoishi:

    for (i = stolen_conn_count; (i < BE.max_conn) && PQisBusy(conni[i]); i++);

    if (i < BE.max_conn)
        return conni[i];
    else {
        printf("Free pgconn not found...");
        return NULL;
    }
//	goto vseravnoishi;
}

int get_free_pgconn_idx() {
    int i;
// vseravnoishi:

    for (i = stolen_conn_count; (i < BE.max_conn) && PQisBusy(conni[i]); i++);

    if (i < BE.max_conn)
        return i;
    else {
//	printf("Free pgconn not found...");	
        return -1;
    }
//	goto vseravnoishi;
}

int backend() {
    int i;
    int n = 0;
    char conn_str[256] = {""};


    if (!(FERR = fopen("./dbtest.err", "w+"))) {
        printf("Cannot open ./dbtest.err \n");
        exit(1);
    }

    srand(time(NULL));

/*    sprintf(conn_str, "%s %s %s %s",
	 ( BE.host[0] != '\0' ? BE.host : "" ),
	 ( BE.user[0] != '\0' ? BE.user : "" ),
	 ( BE.password[0] != '\0' ? BE.password : "" ),
	 ( BE.dbname[0] != '\0' ? BE.dbname : "" ) ); */

    for (i = 0; i < BE.max_conn; i++) {
        conni[i] = PQconnectdb(BE.connect_string);
        if (PQstatus(conni[i]) == CONNECTION_BAD) {
            fprintf(stderr, "Connection to database %s failed. %s",
                    BE.connect_string,
                    PQerrorMessage(conni[i]));
            exit_nicely();
        }
        PQsetnonblocking(conni[i], true);
        pfd[i].fd = PQsocket(conni[i]);
        pfd[i].events = POLLIN;
    }

    qstat = calloc(sizeof(q_stat), i);
    printf("%d connections to postgres established\n", i);
    printf("Log report query format:\n"
                   "[query name]\t avg time / times \t min time / max time \n"
                   "Where: 'avg time' - average query processing time calculated on report_interval\n"
                   "	'times' - query processed times during report_interval\n"
                   "	'min time' - minimal query processing time occured during report_interval\n"
                   "	'max time' - maximum query processing time occured during report_interval\n"
                   " All timings will be in milliseconds!\n ");

    printf("\nStart testing using report_interval = [%d] seconds....\n\n", BE.report_interval);
    sleep(1);
//    last_report_time = time(NULL);  

    limit = BE.query_limit;
    report_interval = BE.report_interval;
    gettimeofday(&test_start_time, NULL);

    q_sheduler();

    timer(NULL);

    while (((n = poll(pfd, BE.max_conn + 1, 1)) >= 0)) {
//        call_timer();
//	if ( n == 0 ) when timeout expired
        for (i = 0; i < BE.max_conn; i++) {
            if (pfd[i].revents == POLLIN) {
                q_result_handle(i);
                processed++;

                if (processed >= limit && limit > 0) {
                    char buf[1024] = {0};
                    FILE *f;
                    struct timeval tvn;
                    timer(&tvn);
                    sprintf(buf, "limit %d sent/processed %d/%d in %ld.%06ld\n",
                            limit, sent, processed,
                            tvn.tv_sec, tvn.tv_usec);

                    fprintf(stderr, buf);
//		    break;
                    log_report();
                    exit_nicely();
                } else {
//		    printf("NOT POLLIN\n");
//		    write(pfd[BE.max_conn].fd," ",1);
                }
            }
        }

//	if (sent > limit) continue;

        for (i = 0; i < BE.max_conn; i++) PQflush(conni[i]);

        q_sheduler();
/*
	if ( qnum >= qdef_cnt ) qnum = 0;
	q_send();
        qnum++;
*/
    }
//    printf("n = %d EINTR = %d\n",n,EINTR);
}

int q_send(q_def *q) {
    PGconn *conn;
    int conn_idx = 0;
    if ((conn_idx = get_free_pgconn_idx()) < 0) {
//    	printf("Free pgconn not found, waiting ...\n");	
        return 0;
    }
//    printf("q_send: min %.3f; max %.3f; \n", q->min_time, q->max_time);
    memset(&qstat[conn_idx], 0, sizeof(q_stat));
    conn = qstat[conn_idx].conn = conni[conn_idx];
    qstat[conn_idx].qdef = q;
    q_clone(&qstat[conn_idx]);
    PQ_SEND_QUERY(qbuf, "q_send()");
    sent++;
    gettimeofday(&(qstat[conn_idx].tv_sent), NULL);
    bs_log(3, BS_LOG_MARK, "%s:%s\n", "q_send()", qbuf);
    return 1;
}

#define MEASURE_MULTIP 1000000
int q_sheduler() {
    struct timeval tvn = {0};
    int passed = 0;
    int forgotten = 0;
    int measure = 0, i = 0;
    gettimeofday(&tvn, NULL);
//    printf("reset stats: passed %d; last %d; \n", passed, last_stat_reset_time);
    time_diff(&test_start_time, &tvn, &tvn);

    passed = (tvn.tv_sec * MEASURE_MULTIP) + (tvn.tv_usec);

    if (passed >= BE.duration * MEASURE_MULTIP) {
        printf("Stop testing due to duration time %d sec. exhausted \n\n ", BE.duration);
        log_report();
        exit_nicely();
    }

    for (i = 0; i < qdef_cnt; i++) {
        q_def *q = &q_defs[i];
        if ((passed >= q->next_send) || q->last_sent == 0) {
            measure = q->measure / q->density;
            forgotten = (passed / measure) - q->sent;
            if (forgotten < 0) continue; // skip sending if already send more than requiered
            if (q_send(q)) q->last_sent = passed;
            while (forgotten > 0 && q_send(q)) { forgotten--; }
            q->forgotten = forgotten;
//     	    printf("passed  %d measure %d forgotten %d\n",passed,measure,forgotten);

            q->next_send = passed + randnotmore((measure));
        }

    }

    if (passed - last_report_time >= (BE.report_interval * MEASURE_MULTIP)) {
        log_report();
        last_report_time = passed;
    }

    if (passed - last_stat_reset_time >= (BE.reset_stat_interval * MEASURE_MULTIP)) {
//        printf("reset stats: passed %d; last %d; \n", passed, last_stat_reset_time);
        for (i = 0; i < qdef_cnt; i++) {
            q_defs[i].min_time = 0;
            q_defs[i].max_time = 0;
            q_defs[i].tv_total.tv_sec = 0;
            q_defs[i].tv_total.tv_usec = 0;
            q_defs[i].sent_stat = 0;
        }
        last_stat_reset_time = passed;
    }
    sheduler_called++;
}

struct timeval *time_diff(struct timeval *tvo,
                          struct timeval *tvn,
                          struct timeval *diff) {
    unsigned long q_tmp = (1000000 * (tvn->tv_sec - tvo->tv_sec)) \
 + tvn->tv_usec - tvo->tv_usec;
    diff->tv_sec = q_tmp / 1000000;
    diff->tv_usec = q_tmp % 1000000;
    return diff;
}

char *q_clone(q_stat *qs) {
    q_def q;
    char *chunk_ptr;
    int i;

    q = *(qs->qdef);
    chunk_ptr = q.sql;

    qbuf[0] = '\0';

    for (i = 0; i < q.v_cnt; i++) {
        strncat(qbuf, chunk_ptr, q.voffs[i]);
        chunk_ptr += q.voffs[i];
        strcat(qbuf, v_clone(q.vdefs[i]));
    }

    strcat(qbuf, chunk_ptr);

    return qbuf;
}

char *q_clone2(q_def *qd) {
    q_def q;
    char *chunk_ptr;
    int i;

    q = *qd;
    chunk_ptr = q.sql;

    qbuf[0] = '\0';

    for (i = 0; i < q.v_cnt; i++) {
        strncat(qbuf, chunk_ptr, q.voffs[i]);
        chunk_ptr += q.voffs[i];
        strcat(qbuf, v_clone(q.vdefs[i]));
    }

    strcat(qbuf, chunk_ptr);

    return qbuf;
}

void mysrand(int add) {
    struct timeval tv;
    gettimeofday(&tv, NULL);
    srand(tv.tv_usec + tv.tv_sec);
}


char *v_clone(v_def *v) {
    numstr[0] = '\0';
    //  mysrand(v->items_count  * 10000);
    // mysrand();    
    switch (v->vtype) {
        case vlist:
        case vfile:
            return v->items[
                    (v->selection == 'R' ? randnotmore((v->items_count)) :
                     ((v->pos < v->items_count) ? (v->pos++) : (v->pos = 0)))];
        case vdate: {
            struct tm *mytm = {0};
            time_t tval = v->min + randnotmore(v->max - v->min);
            mytm = localtime(&tval);
            sprintf(numstr, "%d-%d-%d", 1900 + mytm->tm_year,mytm->tm_mon+1, mytm->tm_mday);
            break;
        }
        case vtime: {
            struct tm *mytm = {0};
            int tval = v->min + randnotmore(v->max - v->min);
            sprintf(numstr, "%d:%d:%d", tval / 3600, (tval % 3600) / 60, (tval % 3600) % 60);
            break;
        }
        case vfragment:

        case vint:
        default:
            if (v->max == 0) v->max = 100000;
//	sprintf(numstr,"%d",(v->min + randnotmore(v->max - v->min)));	
            sprintf(numstr, "%d", (v->min + (v->selection == 'R' ? randnotmore(v->max - v->min) :
                                             ((v->pos < v->max) ? (v->pos++) : (v->pos = 0)))));


    };

    return numstr;
};


int q_result_handle(int i) {
    PGresult *res;
    PGconn *conn = qstat[i].conn;

    PQconsumeInput(conn);

    while ((res = PQgetResult(conn))) {
        if (PQresultStatus(res) != PGRES_COMMAND_OK &&
            PQresultStatus(res) != PGRES_TUPLES_OK) {
            bs_log(1, BS_LOG_MARK, "%s\n", PQerrorMessage(conn));
            qstat[i].qdef->errors++;
        }
    }

    q_upstat(&qstat[i]);

    bs_log(3, BS_LOG_MARK, " Rslt \n", "q_result_handle()");

    return 0;
}


void q_upstat(q_stat *qs) {
    struct timeval tvn = {0};
    struct timeval *total = &qs->qdef->tv_total;
    float q_time = 0;
    suseconds_t qt_sent = 0;
    suseconds_t now = 0;
    unsigned long q_tmp = 0;

    gettimeofday(&tvn, NULL);
    q_tmp = (1000000 * (total->tv_sec + tvn.tv_sec - qs->tv_sent.tv_sec))
            + tvn.tv_usec - qs->tv_sent.tv_usec + total->tv_usec;
    total->tv_sec = q_tmp / 1000000;
    total->tv_usec = q_tmp % 1000000;

/*    printf("mytv_sec %d mytv_usec %d\n",
    qs->qdef->tv_total.tv_sec,
    qs->qdef->tv_total.tv_usec); */

    q_time = (tvn.tv_sec - qs->tv_sent.tv_sec) * 1000
             + (((float) (tvn.tv_usec - qs->tv_sent.tv_usec)) / 1000);

//    printf("mytv_ms %.3f \n", q_time);
    
//    printf("previous: min %.3f; max %.3f; \n", qs->qdef->min_time, qs->qdef->max_time);

    if (q_time < qs->qdef->min_time || !qs->qdef->min_time) {
        qs->qdef->min_time = q_time;
    }

    if (q_time > qs->qdef->max_time || !qs->qdef->max_time) {
        qs->qdef->max_time = q_time;
    }

    qs->qdef->sent++;
    qs->qdef->sent_stat++;
//    printf("q_time %.3f; min %.3f; max %.3f; \n", q_time, qs->qdef->min_time, qs->qdef->max_time);
    q_sheduler();

    return;
}

/*
void call_timer()
{
    int current_time = time(NULL);

    if ( current_time - last_report_time >= report_interval ) {
	log_report();	
    	last_report_time = current_time;
    }
}
*/

void exit_nicely() {
    int i;
    for (i = 0; i < BE.max_conn; i++) PQfinish(conni[i]);
    for (i = 0; i < vdef_cnt; i++) free(vdefs[i]);
    exit(0);
}

void log_report() {
    int i;
    unsigned long avg_microsec = 0;
    struct timeval tvn;
    for (i = 0; i < qdef_cnt; i++) {
        struct timeval total = q_defs[i].tv_total;
        avg_microsec = (total.tv_sec * 1000000 + total.tv_usec) / (!q_defs[i].sent_stat ? 1 : q_defs[i].sent_stat);
        tvn.tv_sec = avg_microsec / 1000000;
        tvn.tv_usec = avg_microsec % 1000000;

#define MEASURE_MS

#ifdef MEASURE_MS
        printf("[%s]\t%ld.%03ld ms / %ld\t%.3f ms / %.3f ms\tforgot %d\n",
               q_defs[i].name,
               (tvn.tv_sec * 1000 + tvn.tv_usec / 1000),
               tvn.tv_usec % 1000,
               q_defs[i].sent_stat,
               q_defs[i].min_time,
               q_defs[i].max_time,
               q_defs[i].forgotten);
#else
        printf("[%s]\t %d.%06d ,sent %d sheduler called %d\n",
            q_defs[i].name,
            i,
            tvn.tv_sec,
            tvn.tv_usec,
            q_defs[i].sent_stat,
            sheduler_called);
#endif

    }

    timer(&tvn);

    printf("log_report: sent/processed %d/%d in %ld.%06ld\n", sent, processed,
           tvn.tv_sec, tvn.tv_usec);
}

int q_send_simple() {
    PGconn *conn;
    int conn_idx = 0;

    if ((conn_idx = get_free_pgconn_idx()) < 0) {
        return 0;
        printf("Free pgconn not found, waiting ...\n");
    }

    conn = qstat[conn_idx].conn = conni[conn_idx];
    qstat[conn_idx].qdef = &q_defs[qnum];
    q_clone(&qstat[qnum]);
    PQ_SEND_QUERY(qbuf, "q_send()");
    sent++;
    gettimeofday(&qstat[conn_idx].tv_sent, NULL);
    bs_log(3, BS_LOG_MARK, "%s:%s\n", "q_send()", qbuf);
    return qnum;
}



