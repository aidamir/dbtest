%option yylineno
%option never-interactive
/*%option array*/
/*%option stack*/
%option noyywrap
/*%option debug*/
%{

/* Flex specific notes:
    It is possible to redirect flex output and stdout to different streams
*/


#define YY_DECL int yylex(VariableSpace p_vs)

#define SAFE_BEGIN(c) \
			HotrepError("\nstarting cond=%d\n",c); \
		        BEGIN(c); \
			curr_report.old_cond = curr_report.cond; \
			curr_report.cond = c;

#define OPLENLIM 46
/*
    The marcos coded below are not support space between operand and its value
    such as
    [# begin section name] - not proper
    [#begin setction_name] - proper
*/
#define SET_LABEL(op) \
    		{   int i = sizeof((op)); \
    		    int len = 0; \
    		    char* start; \
    		    memset(label,0,OPLENLIM); \
    		    while ( !isalpha(yytext[i]) && yytext[i] != '_' ) i++; \
    		    start = &yytext[i]; \
    		    while ( isalpha(yytext[i]) || yytext[i] == '_' ) {i++;len++;} \
    		    strncpy(label,start,len); \
    		    label[len]='\0'; }

#define SET_VARNAME(op) \
    		{   int i = sizeof((op)); \
    		    int len = 0; \
    		    char* start; \
    		    memset(varname,0,OPLENLIM); \
    		    while ( !isalpha(yytext[i]) yytext[i] == '_' ) i++; \
    		    start = &yytext[i]; \
    		    while ( isalpha(yytext[i]) ) {i++;len++;} \
    		    strncpy(varname,start,len); \
    		    varname[len]='\0'; }

#define EXTRACT_EXPRESSION(op,storage) \
    		    int i = sizeof((op)); \
    		    int len = 0; \
    		    char* start; \
    		    (storage) = calloc(yyleng,1); \
    		    while ( !isalpha(yytext[i]) ) i++; \
    		    start = &yytext[i]; \
    		    while ( yytext[i] != '#' && yytext[i+1] != ']' ) {i++;len++;} \
    		    strncpy((storage),start,len); \
    		    storage[len]='\0';

#define MAKE_BUF_FROM_CHUNK(bf) \
    			{   long buf_size = yy_cp - chunk_start - yyleng; \
    			    prev_buffer = YY_CURRENT_BUFFER;  \
    			    bf = malloc(buf_size+1); \
    			    memset(bf,'\0',buf_size); \
    			    strncpy(bf,chunk_start,buf_size); \
    			    bf[buf_size]='\0'; }

#define ADD_TEXT_TO_BUF(bf,chk) \
    			{ \
    			    long buf_size = strlen(bf) + strlen(chk); \
    			    char* tmp = malloc(buf_size+1); \
    			    memset(tmp,'\0',buf_size); \
    			    prev_buffer = YY_CURRENT_BUFFER; \
    			    strcpy(tmp,bf); \
    			    strcat(tmp,chk); \
    			    free(bf); \
    			    bf = tmp; \
    			    bf[buf_size]='\0'; }

#define ADD_CHUNK_TO_BUF(bf) \
    			{ \
    			    long buf_size = (strlen(bf) + (yy_cp - chunk_start - yyleng)); \
    			    char* tmp = malloc(buf_size+1); \
    			    memset(tmp,'\0',buf_size+1); \
    			    prev_buffer = YY_CURRENT_BUFFER; \
    			    strncpy(tmp,bf,strlen(bf)); \
    			    strncat(tmp,chunk_start,(yy_cp - chunk_start - yyleng)); \
    			    free(bf); \
    			    bf = tmp; \
    			    bf[buf_size]='\0'; }

%}

ws    [ \t]
nonws [^ \t\n\<\>\.\,\;\)\(\?\!\"\'\&\\]
pt [.,:;)(?!"']
bs ([\\])*([^ \t\n<])+
doctype ("<!DOCTYPE"+([^\>]+)*">"){1}
start_comment "<!--"{1}
end_comment "-->"{1}
comment {start_comment}{1}+[^{end_comment}]*{end_comment}{1}
predl "."+{ws}*[A-Z]+
nbsp "&nbsp"

/* free text handling rules */

glas [����������]|[���������]
sogl [����������������������]|[���������������������]

/*post {ws}+({sogl}|{glas}){2}+{glas}+*/

rusword ({sogl}|{glas})+
engword	([A-Z]|[a-z])+

anyword {rusword}|{engword}

/* generate reports from samples and SQL query  */

ops "[#"
ope "#]"


var {ops}+{varname}{ope}+

varname [A-Za-z\_]+


/* block operators */


begin {ops}{1}{ws}*[Bb]+[Ee]+[Gg]+[Ii]+[Nn]+[ \n\t]*{varname}+{ope}{1}

end {ops}{1}{ws}*[Ee]+[Nn]+[Dd]+[ \n\t]*{varname}+{ope}{1}

ifdef {ops}{1}{ws}*[Ii]+[fF]+[Dd]+[Ee]+[fF]+[ \n\t]*{varname}{ope}{1}

if {ops}{1}{ws}*[Ii]+[fF]+[ \n\t]*{varname}[ \n\t]*[=]{2}[ \n\t]*
/*else {ops}{1}{ws}*[Ee]+[Ll]+[Ss]+[Ee]+[ \n\t]*[A-Za-z]+{ope}{1}*/
else {ops}{1}{ws}*[Ee]+[Ll]+[Ss]+[Ee]+{ws}*{ope}{1}

endif {ops}{1}{ws}*[Ee]+[Nn]+[Dd]+[Ii]+[fF]+[ \n\t]*[A-Za-z]+{ope}{1}

/*set {ops}{1}{ws}*[Ss]+[Ee]+[Tt]+[ \n\t]*{left_op}[ \n\t]*[=]{1}[ \n\t]*{rigth_op}+{ope}{1}*/
set {ops}{1}{ws}*[Ss]+[Ee]+[Tt]+[ \n\t]*{varname}[ \n\t]*[=]{1}[ \n\t]*
/*if {ops}{1}{ws}*[Ii]+[fF]+[ \n\t]*{var}+[ \n\t]*[==]+[ \n\t]*[A-Za-z]+{ope}{1}*/

showenv {ops}{1}{ws}*[Ss]+[Hh]+[Oo]+[Ww]+[Ee]+[Nn]+[Vv]+{ws}*{ope}{1}

/* sections end/start def  (prescan condition)  */

section_start "#{"{1}
section_end "#}"{1}

/* start section followed section name */

section_prescan {section_start}[Pp]+[Rr]+[Ee]+[Ss]+[Cc]+[Aa]+[Nn]+
section_row {section_start}[Rr]+[Oo]+[Ww]+
section_pheader {section_start}[Hh]+[Ee]+[Aa]+[Dd]+[Ee]+[Rr]+
section_pfooter {section_start}[Ff]+[Oo]+[Tt]+[Ee]+[Rr]+


/*begin_report_body {ops}{1}{ws}*[Bb]+[Ee]+[Gg]+[Ii]+[Nn]+[ \n\t]*[Bb]+[Oo]+[Dd]+[Yy]+{ope}{1}
end_report_body {ops}{1}{ws}*[Ee]+[Nn]+[Dd]+[ \n\t]*[Bb]+[Oo]+[Dd]+[Yy]+{ope}{1}*/

/* operands and operator <extract_operands> rule set*/

oper {op_set}|{op_eqaul}
op_set ([ \n\t]*[=]{1}[ \n\t]*){1}
op_equal ([ \n\t]*[=]{2}[ \n\t]*){1}

/* Conditions */

%x comment
%x seekend
%x not_defined
%x prescan_0
%x prescan
%x var_value

%{
#include <locale.h>
#include <stdarg.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
//#include "variables.h"
#include "rep.h"

/* Global definitions */

report curr_report;

VariableSpace main_vs;

int recno;

int pages_generated;

/*extern void dbconnect();
extern int ExecSQL(const char* query);*/

typedef enum _myoperators {equal,set,delete} operators;

static int cond_called_from_global=0;

static const char* progname = "hotrep";

static const char* section;

char*    dontforgetfree=NULL;

int NewPage(report* rep);
void SetNextPageFnameDefault(report* rep);

YY_BUFFER_STATE text_buffer;
YY_BUFFER_STATE main_buffer;

#define strdup ayytextdup;

#define STRCPYRMQ(dest,src,len) \
    		if ( (src)[0]=='"' ) \
    		{ \
    		    strncpy((dest),&(src)[1],(len)-2); \
    		    (dest)[len-3]='\0'; \
    		} else strcpy((dest),(src));

//#define DBG

#ifdef DBG
    void dbg( const char* fmt, ... )
    {
    	va_list ap;
    	printf("\nsrcline:[%d]",yylineno);
    	    va_start(ap,fmt);
    	    	vprintf(fmt,ap);
    	    va_end;
    }
#else
    void dbg( const char* fmt, ... ){};
#endif

// strdup causes SIGSEGV ????

char* ayytextdup(char* str)
{
    	    char* buf=malloc(yyleng+2);
    	    if ( buf == NULL ) {
    		 printf("\nyytextdup: malloc failed");
    		 exit(1);
    	    }
    	    memset(buf,'\0',yyleng+2);
    	    strcpy(buf,str);
    	    return buf;
}

%}

%%
    int sublevelstart = 0;
    int sublevelend = 0;
    int not_def = 0;
    char label[46];
    char varname[46];
    YY_BUFFER_STATE prev_buffer;
    YY_BUFFER_STATE buffer;
    char* chunk_start = 0;
    int cond_yylex = 0;
    long chunk_size = 0;
    int see_old_value = 0;
    operators curr_op = equal;
    char** buf_ptr = NULL;
    char* bfv=NULL;


<*>{ops}[ ]+ { printf("Syntax error: No such operators starting with spaces\n"); }

<prescan_0>{section_prescan} {


#define DEF_SECT_BODY if ( buf_ptr != NULL ) MAKE_BUF_FROM_CHUNK((*buf_ptr));\

    			      DEF_SECT_BODY

    			      chunk_start = yy_cp;
    			      buf_ptr = &curr_report.pre_scan;
    				    dbg("\ncurr_report \n");
    			      }

<prescan_0>{section_pheader} {
    			      DEF_SECT_BODY
    			      chunk_start = yy_cp;
    				    dbg("curr_report %s\n",*buf_ptr);
    			      buf_ptr = &curr_report.pheader;
    			    }
<prescan_0>{section_row} {
    			      DEF_SECT_BODY
    				if ( buf_ptr != NULL )
    				    MAKE_BUF_FROM_CHUNK((*buf_ptr))
    			      chunk_start = yy_cp;
//	    dbg("curr_report %s\n",buf_ptr);
    			    dbg("curr_report %s\n",*buf_ptr);
    			      buf_ptr = &curr_report.row;
    			      }

<prescan_0>{section_pfooter} {
    			      DEF_SECT_BODY
    			      chunk_start = yy_cp;
    			    dbg("curr_report %s\n",*buf_ptr);
    			      buf_ptr = &curr_report.pfooter;
    			      }

<prescan_0>{section_end} {
    			    DEF_SECT_BODY
    			    chunk_start = NULL;
    			    buf_ptr = NULL;
    			}
<prescan>{set} 	{
    			    SET_LABEL("[#set");
    			    curr_op = set;
    			    SAFE_BEGIN(var_value);
    			    chunk_start = yy_cp;
    		}
<*>{showenv} {
		show_env(curr_report.main_vs);
	    }
{begin} {
    	    SET_LABEL("[#begin");
    	    buffer = YY_CURRENT_BUFFER;
    dbg("\nfound begin:%s\n yy_cp = %d",label,yy_cp);
    	    chunk_start = yy_cp;
    	    SAFE_BEGIN(seekend);
    	}

<seekend>{end} {
    			see_old_value = 0;

    			if ( strstr(yytext,label) )
    			{
    		    	    int i=0;
    			    VariableSpace vs;
    			    long buf_size = yy_cp - chunk_start - yyleng;

    		dbg("\n EEEE end found%s\n",label);
    			    prev_buffer = YY_CURRENT_BUFFER;

    			    vs = GetVarspace(main_vs,label);

    			    if (vs == NULL)  {
    				printf("Variable [%s] not found! Exiting... \n\n",label);
    				exit(0); 
			    }
    			    
			    curr_report.BeginBlockVal[ curr_report.depth++ ] = vs;
			    
			    do
    			    {
    			    	char* bf = malloc(buf_size+1);
    	        dbg("\n CURR ITEM ====   %s",vs->value);
    				memset(bf,'\0',buf_size);
    				strncpy(bf,chunk_start,buf_size);
    				bf[buf_size]='\0';
    		dbg("\nbuffer >>> %s <<<",bf);

    				yy_scan_string(bf);
    				SAFE_BEGIN(INITIAL);
				
				yylex(vs);

/*	!!!		GO ONE RECORD FORWARD   !!!	*/

    				if (curr_report.go_forward)
				{
				    curr_report.recno = forward(main_vs);

    				    curr_report.cur_row++;
    				
				    if ( ( (++curr_report.cur_prow) == curr_report.page_rows ) )
    				    {
					curr_report.cur_page++;
					NewPage(&curr_report);
    					curr_report.cur_prow = 0;
    				    }

				}
    		dbg("\nWHILE Val = %s, Oldval = %s\n",	p_vs->value, p_vs->old_value);

    			    } while ( BeginBlockIsEnd(&curr_report) );
			    
			    curr_report.depth--;

//			    free(bf);
    			    see_old_value = 1;
			    HotrepError("switch\n");
    			    yy_switch_to_buffer(prev_buffer);
    			    
			    SAFE_BEGIN(INITIAL);

    			}

    		}

<not_defined>{endif} {
    			if ( strstr(yytext,label) )
    			    SAFE_BEGIN(curr_report.old_cond);
    	     	    	    not_def  =  0;
    		     }

<not_defined>{else} {
    			    SAFE_BEGIN(curr_report.old_cond);
    	     	    	    not_def  =  0;
    		     }

<seekend,not_defined,var_value,prescan_0,prescan>(.{1})|(\n{1}) {   }

<INITIAL,prescan>{ifdef} {
    	    SET_LABEL("[#ifdef");
/*    	printf("\n IFDEF \n"); 
       dbg("ifdef label = %s\n",label);*/
    	    if (!GetVariableBool(main_vs,label)) 
	    { 
		SAFE_BEGIN(not_defined);
		not_def = 1;
	    }
    	}

<INITIAL,prescan>{else} {
//	   if ( strstr(yytext,label) )
    	        SAFE_BEGIN(not_defined);
    		not_def = 1;
    	}

<INITIAL,prescan>{endif} { if (not_def) 
			    { SAFE_BEGIN(curr_report.old_cond);
				not_def = 0; }
			 }

{set} 	{
    			    SET_LABEL("[#set");
    			    curr_op = set;
    			    SAFE_BEGIN(var_value);
    			    chunk_start = yy_cp;
    	}

{if} 	{
    	    SET_LABEL("[#if");
    	    curr_op = equal;
    	    SAFE_BEGIN(var_value);
    	    chunk_start = yy_cp;
    	}

<var_value>{var} {
/* [#set var = something [#var#] something #] */
    		    char tmp[256]={"\0"};
    		    char* value;
    		    strncpy(tmp,&yytext[2],( strlen(yytext)-4 ));
    		    tmp[strlen(yytext)-2]='\0';
    		    if ( (value = GetVariable(main_vs,tmp)) != NULL )
    		    {
    			    long buf_size = ( (bfv) ? strlen(bfv) : 0 ) \
					     + strlen(value) \
					     + (yy_cp - chunk_start - yyleng);
    			    char* tmp1 = malloc(buf_size+1);
    			    memset(tmp1,'\0',buf_size);
    			    if (bfv) strcpy(tmp1,bfv); 
    			    strncat(tmp1,chunk_start, (yy_cp - chunk_start - yyleng));
			    strcat(tmp1,value);
    			    prev_buffer = YY_CURRENT_BUFFER;
			    bfv = tmp1;
    		    }
    		    chunk_start = yy_cp;
		}

<var_value>{ope} {
//    		    char* bf_ = buf;

    		    if ( bfv != NULL )
    		    {
    			ADD_CHUNK_TO_BUF(bfv);
    		    } else {
    		    	 MAKE_BUF_FROM_CHUNK(bfv);
    		    }
        dbg("\n VALUE BUFFER =   %s",bfv);

/*		    memset(bf,'\0',buf_size);
    		    strncpy(bf,chunk_start,buf_size);
    		    bf[buf_size]='\0';*/

    		    switch(curr_op)
    		    {
    			case set:
    			    SetVariable(main_vs,label,bfv);
//				printf("cond_called_from %d old %d",curr_report.cond,curr_report.old_cond);
    				SAFE_BEGIN(curr_report.old_cond);
			    break;
    			case equal:
//		printf("comparing %s == %s\n",GetVariable(main_vs,label),bf);
    			    if ( strcmp(GetVariable(main_vs,label),bfv) != 0 )
    			    {
    				SAFE_BEGIN(not_defined);
    				not_def = 1;
//			    	printf("comparing 2 %s == %s\n",GetVariable(main_vs,label),bf);
    			    }
    			    break;
    			default:
    				SAFE_BEGIN(curr_report.old_cond);
    		    };
    		    free(bfv);
    		    bfv = NULL;
//		    chunk_start = yy_cp;
		}

{var} { /* seek for a var and put it on the stream */
    	    char tmp[256]={"\0"};
    	    strncpy(tmp,&yytext[2],( strlen(yytext)-4 ));
    	    tmp[strlen(yytext)-2]='\0';
    	    if ( ! see_old_value )
    	    {
    		if (GetVariableBool(main_vs,tmp))
    		    printf("%s",GetVariable(main_vs,tmp));
    	    } else {
    		if (GetVariableBoolOld(main_vs,tmp))
    		    printf("%s",GetVariableOld(main_vs,tmp));
    	    }
    	}

<*><<EOF>> {
	    if ( curr_report.cur_section == prescan_0 )
	    {
		if ( buf_ptr != NULL ) 
		{
    		    DEF_SECT_BODY
    		    chunk_start = NULL;
    		    buf_ptr = NULL;
		} else if ( !(curr_report.pre_scan) 
			&& !(curr_report.row) 
			&& !(curr_report.pheader) 
			&& !(curr_report.pfooter) )
		{
		    HotrepError("Nothing section defined. Can't create report. Exiting...." );
		    exit(0);		    
		}
		buf_ptr = NULL;
	    }

    	    yyterminate();
    	}
%%


int NewPage(report* rep)
{
    if ( !rep->cur_page ) return 0;
/* ���� � ������� �� ���������� ���������� PAGE_FNAME_NEXT 
�� ���������� �� */
        if (!GetVariableBool(rep->main_vs,"PAGE_FNAME_NEXT"))
	     SetNextPageFnameDefault(rep);
    	
	if ( rep->pfooter != NULL )
    	{
/* ����� ����������� PAGE_FNAME_NEXT � footer*/	    
	    yy_scan_string(rep->pfooter);
    	    yylex(main_vs);
    	}

    	if (GetVariableBool(rep->main_vs,"PAGE_FNAME_NEXT"))
	    SetRepFout(rep,GetVariable(rep->main_vs,"PAGE_FNAME_NEXT"));
		
/* ������� ���������� �� ����� �������� */
	if (GetVariableBool(rep->main_vs,"PAGE_FNAME_NEXT"))
	    DeleteVariable(rep->main_vs,"PAGE_FNAME_NEXT");

	if ( rep->pheader != NULL )
    	{
    	    yy_scan_string(rep->pheader);
    	    yylex(main_vs);
    	}

	return rep->cur_page;
};


void show_env(VariableSpace space)
{
	struct _variable *current;

	if (!space)
		return NULL;

	for (current = space; current; current = current->next)
	{
#ifdef USE_ASSERT_CHECKING
		assert(current->name);
		assert(current->value);
#endif
		fprintf(stderr,"\n %s.value={%s}, %s.old_value=%s",
					current->name,
					current->value,
					current->name,
					current->old_value);

	}
};


int CreateReport_Vs( VariableSpace vs, void* conn)
{
    if ( vs == NULL ) return 0;

    main_vs = vs;

    CreateReport(GetVariable(vs,"PAGE_ROWS"),
    	    GetVariable(vs,"REPFNAME"),
    	    GetVariable(vs,"OUTFNAME"),
    	    NULL,
    	    conn);
};

int CreateReport(int record_per_page,
    	    const char* repf_name,
    	    const char* outf_name,
    	    const char* SQL,
    	    void* conn_ptr)
{
extern int errno;

    FILE* report_def = NULL;
    
    if (repf_name == NULL)
    {
    	HotrepError("No report file specified. Can't create report.\n");
    	return 0;
    }

    if ( ( report_def = fopen(repf_name,"r") ) )
    {
	yyin = report_def;
    } else {
    	HotrepError("%s:%s",repf_name,strerror(errno));
	return 0;
    }
    
/*    if (outf_name != NULL)
    {
        yyout = stdout = fopen(outf_name,"w");
    } else curr_report.outf_name = stdout; */

    memset(&curr_report,0,sizeof(curr_report));

    if (main_vs == NULL) curr_report.main_vs = main_vs = CreateVariableSpace();
    else curr_report.main_vs = main_vs;

    curr_report.fout_name = (char*) outf_name;

// SCAN 0 - extract report sections
	
    curr_report.cur_section = prescan_0;

	SAFE_BEGIN(prescan_0);

    		yylex(main_vs);

    curr_report.cur_section = prescan;

        SAFE_BEGIN(prescan);

// SCAN 1 - setup

    if ( curr_report.pre_scan != NULL )
    {
	yy_scan_string(curr_report.pre_scan);
        yylex(main_vs);
    }

    if ( !GetVariableBool(main_vs,"SQL") && SQL == NULL )
    {
    	 printf("Variable SQL not defined. Can't create report. Exiting...\n");
         DestroyVariableSpace(main_vs);
    	 exit(0);
    }

    if ( curr_report.row == NULL) {
	printf(" row pattern is not specified, printing report environment and exiting...\n");
	show_env(main_vs);
	exit(0);    
    }

    if ( conn_ptr == NULL) dbconnect(main_vs); 
    else
    { 
	set_conn(conn_ptr);
	set_transhandle_off();
    }
    
    if ( GetVariableBool(main_vs,"SQL") )
    	ExecSQL(main_vs,GetVariable(main_vs,"SQL"));
    else ExecSQL(main_vs,SQL);

    curr_report.cur_page = 0;

    curr_report.cur_prow = 0;

    if (GetVariableBool(main_vs,"PAGE_ROWS"))
    {
	curr_report.page_rows = atoi(GetVariable(main_vs,"PAGE_ROWS"));
    	if ( strstr( GetVariable(main_vs,"OUTFNAME"),"[page_no]" ) )
	{ 
	    SetVariable( main_vs,"PAGE_FNAME_MASK", GetVariable( main_vs,"OUTFNAME") );
	    SetNextPageFnameDefault(&curr_report);
	    SetRepFout( &curr_report, GetVariable(main_vs,"PAGE_FNAME_NEXT") );
	    DeleteVariable(main_vs,"PAGE_FNAME_NEXT");
	}
    } else if (outf_name != NULL) yyout = stdout = fopen(outf_name,"w");
 
// SCAN query result rows - row section.
    
    while ( forward(main_vs) )
    {
        curr_report.cur_section = INITIAL;
    
    	SAFE_BEGIN(0);
    
    	yy_scan_string(curr_report.row);
    
        curr_report.go_forward = true;
	
	yylex(main_vs);
    };
    
    DestroyVariableSpace(main_vs);

}

void
HotrepError(const char *fmt,...)
{
	va_list	ap;

	fflush(stdout);
	if (curr_report.fout != stdout)
		fflush(curr_report.fout);

	if (curr_report.fin_name)
		fprintf(stderr, "%s:%s:%u:",progname,curr_report.fin_name,yylineno);
	else fprintf(stderr, "\n%s:",progname);
	
	va_start(ap, fmt);
	vfprintf(stderr, fmt, ap);
	va_end(ap);
	fprintf(stderr,"\n");
}


bool BeginBlockIsEnd(report *rep)
{
int i = 0;
int depth = ( (rep->depth > 1) ? rep->depth - 1 : rep->depth );

    for (i ; i < depth ; i++)
    {
	if ( strcmp( (rep->BeginBlockVal[i])->value,
		     (rep->BeginBlockVal[i])->old_value ) ) 
		     {
		        if ( (depth - i) > 1 ) rep->go_forward = false;
			else rep->go_forward = true;
			return false;
		     }
    }
    
    rep->go_forward = true;
    return true;
}


bool SetRepFout(report *rep, const char* fname)
{
extern int errno;
    
    if ( rep->fout ) fclose(rep->fout);

    if ( rep->fout = yyout = stdout = fopen(fname,"w") )
	SetVariable(rep->main_vs,"OUTFNAME",fname);
    else {  
	HotrepError(strerror(errno));
	exit(0);
    }
}

void SetNextPageFnameDefault(report* rep)
{
char* ptr;
char* ret;
char tmp[1024]={"\0"};
char* pn_pos;

    if (GetVariableBool(rep->main_vs,"PAGE_FNAME_MASK"))
	ptr = GetVariable(rep->main_vs,"PAGE_FNAME_MASK");
    else ptr = GetVariable(rep->main_vs,"OUTFNAME");

    if ( pn_pos = strstr(ptr,"[page_no]") )
    {
	strncpy(tmp,ptr,(pn_pos - ptr));
	if ( rep->cur_page )
	    sprintf(tmp,"%s%d%s",tmp,rep->cur_page,
	    ( pn_pos + sizeof("[page_no]") - 1 ));
	else sprintf(tmp,"%s%s",tmp,( pn_pos + sizeof("[page_no]") - 1 ));

    } else sprintf( tmp,"%s%d",ptr,rep->cur_page );

    SetVariable(rep->main_vs,"PAGE_FNAME_NEXT",tmp); 

    return;
}

const char* IntToStr(int num)
{
    char tmp[64]={"\0"};
    char* ret = NULL;
    sprintf(tmp,"%d",num);
    ret = malloc(strlen(tmp));
    strcat(ret,tmp);
    return (const char*)ret;
}

