#ifndef __BEOS__

#ifndef __cplusplus
#ifndef bool
typedef char bool;

#endif	 /* ndef bool */
#endif	 /* not C++ */

#ifndef true
#define true	((bool) 1)
#endif

#ifndef false
#define false	((bool) 0)
#endif

#endif	 /* __BEOS__ */

typedef bool *BoolPtr;

#ifndef TRUE
#define TRUE	1
#endif

#ifndef FALSE
#define FALSE	0
#endif

/*
 * NULL
 *		Null pointer.
 */
#ifndef NULL
#define NULL	((void *) 0)
#endif
