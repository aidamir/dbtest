#include "ramdb.h"
#include "libpq-fe.h"
#include <sys/time.h>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>
#include "util.h"
#include "c.h"

int max_id = 6000;
int max_items = 21;


/*
void exit_nicely(PGconn *conn)
{
    PQfinish(conn);
    exit(1);
}
*/

int randnotmore(int interval)
{
    return  0 + (int) ( (float)(interval) * rand() / ( RAND_MAX + 1.0 ) );
};

struct timeval* timer( struct timeval* tvn )
{
    static struct timeval tv = { 0 };
    unsigned long q_tmp = 0;
    if ( tvn == NULL ) { gettimeofday(&tv,NULL); }
    else {
    	gettimeofday(tvn,NULL);
	q_tmp = (1000000 * ( tvn->tv_sec - tv.tv_sec )) \
			    + tvn->tv_usec - tv.tv_usec;
        tvn->tv_sec  = q_tmp / 1000000;
	tvn->tv_usec = q_tmp % 1000000;
    }
    return tvn;
}

char* gen_query(char* sqlbuf)
{
int i;

    sprintf( ( sqlbuf + strlen(sqlbuf) ),"a%dl0/0(%d",max_items,randnotmore(max_id));

    for ( i = 0; i < max_items ; i++ )
    {
	sprintf( ( sqlbuf + strlen(sqlbuf) ),",%d",randnotmore(max_id) );
    }

    strcat(sqlbuf,"|");

    return sqlbuf;
}

#define BID_LIMIT 1

char* gen_query2(char* sqlbuf)
{
static int bid;
int i;
int items = 3;
bool ab = false;
char tmp[2048] = { '\0' };

char part2[1024]= {  '\0' };

    sprintf(tmp,"%d",randnotmore(max_id));
    sprintf(part2,"0");

    items += randnotmore(max_items);

    for ( i = 0; i < items ; i++ )
    {
	sprintf( ( tmp + strlen(tmp) ),",%d",randnotmore(max_id) );
	sprintf( ( part2 + strlen(part2) ),",0");    
    }

    sprintf(sqlbuf,"%s %c%dl%d/%d(%s|%s|%s)",
				    "285254",
				    ab ? 'a' : 'b',
				    items,
				    strlen(tmp),
				    ( strlen(part2) * 2 ),
				    tmp,
				    part2,
				    part2);
    
    
    if (ab)  ab = false;
    else ab = true;
    
    if ( bid == BID_LIMIT) 
    {
    	sprintf( ( sqlbuf + strlen(sqlbuf) )," c(%d|%d)",
						randnotmore(max_id),
						randnotmore(100000) );
	bid = 0;
    } else bid++;
    
    return sqlbuf;
}

int int_compare(const void *pa, const void *pb)
{
      if(*(int *)pa < *(int *)pb) return -1;
      if(*(int *)pa > *(int *)pb) return 1;
      return 0;
}


lotlistitem* lotlist_addtohead(lotlist* llst,lotinfo* in)
{
    lotlistitem* tmp = llst->head;
    lotlistitem* new_item;
    
    new_item = (lotlistitem*)calloc(1,sizeof(lotlistitem));

	    new_item->lp = in;

	    llst->head = new_item;
    
	    new_item->next = tmp;
    
	    llst->items++;
	    
	    return new_item;
}

lotlistitem* lotlist_rm(lotlist* llst)
{
lotlistitem* tmp = llst->head;
    	    
	    if ( tmp == NULL ) return NULL;
	    	    
	    llst->head = llst->head->next;
    
	    llst->items--;

	    free(tmp);

	    return llst->head;
}

logonlistitem* logonlist_addtohead(logonlist* llst,account* in)
{
    logonlistitem* tmp = llst->head;
    logonlistitem* new_item;
    
    new_item = (logonlistitem*)calloc(1,sizeof(logonlistitem));

	    new_item->lp = in;

	    llst->head = new_item;
    
	    new_item->next = tmp;
    
	    llst->items++;
	    
	    return new_item;
}

logonlistitem* logonlist_rm(logonlist* llst)
{
logonlistitem* tmp = llst->head;
    	    
	    if ( tmp == NULL ) return NULL;
	    	    
	    llst->head = llst->head->next;
    
	    llst->items--;

	    free(tmp);

	    return llst->head;
}

/*
lotlistitem* lotlist_addtotail(lotlist* llst,lotinfo* in)
{
    lotlistitem* tmp = llst->tail;
    lotlistitem* new_item;
    
	    if (tmp == NULL) 
		return lotlist_addtohead(llst,in);
        
	    new_item = (lotlistitem*)calloc(1,sizeof(lotlistitem));
	    
	    new_item->lp = in;
	    
	    llst->tail->next = new_item;
    
	    llst->tail = new_item;
    
	    llst->items++;

	    return llst->tail;
}


logonlistitem* logonlist_addtotail(logonlist* llst,account* in)
{
    logonlistitem* tmp = llst->tail;
    logonlistitem* new_item;
    
	    if (tmp == NULL) 
		return logonlist_addtohead(llst,in);
        
	    new_item = (logonlistitem*)calloc(1,sizeof(logonlistitem));
	    
	    new_item->lp = in;
	    
	    llst->tail->next = new_item;
    
	    llst->tail = new_item;
    
	    llst->items++;

	    return llst->tail;
}*/
