FROM debian:stable-slim
WORKDIR /dbtest

COPY dbtest .
COPY config.xml .
COPY vals ./vals
RUN apt-get update && apt-get -y install postgresql-client openssh-client net-tools nano
CMD exec /bin/bash -c "trap : TERM INT; sleep infinity & wait"
