#include <sys/timeb.h>
#include <unistd.h>
#include <stdio.h>


#define HOURS_FROM_MSEC(Msec)((((Msec)/1000)/60)/60)
#define MINUTES_FROM_MSEC(Msec)(((Msec)/60000)-(((Msec)/60000)/60)*60)
#define SECONDS_FROM_MSEC(Msec)(((Msec)/1000)-(((Msec/1000)/60)*60))
#define MSEC_FROM_MSEC(Msec)((Msec)-(((Msec)/1000)*1000))

#define PRINT_TIME(t,m)	printf("\n %s: %u.%u \n\n\n",(m),\
		SECONDS_FROM_MSEC((t)),MSEC_FROM_MSEC((t)));

/* ADD_ZERO adds zero simbol to the string that will be passed as argument to the printf
* "0%d" if the first atrgument - a less than 10*/

#define ADD_ZERO(a,t) if ((a)<10) strcat((t),"0%d"); \
			else strcat((t),"%d")

#define TIME_S(a,t,ts) \
        *t='\0';*ts='\0'; \
	ADD_ZERO((a[0]),t); \
	strcat((t),":"); \
	ADD_ZERO((a[1]),t); \
	strcat((t),":"); \
	ADD_ZERO((a[2]),t); \
	sprintf((ts),(t),(a[0]),(a[1]),(a[2])); 

#define CUR_TIME_STR(ptr) do { \
			    int tim[3];\
			    char format_str[10] = "\0";\
			    struct tm *cur_time;\
			    time_t t = time(NULL);\
    	    			cur_time = localtime(&t);\
	    			tim[0] = cur_time->tm_hour;\
		    		tim[1] = cur_time->tm_min;\
				tim[2] = cur_time->tm_sec;\
				TIME_S(tim,format_str,(ptr));\
			} while (0);


/* without time() call */
#define CTIMES(ptr,t) do { \
			    int tim[3];\
			    char format_str[10] = "\0";\
			    struct tm *cur_time;\
    	    			cur_time = localtime(&t);\
	    			tim[0] = cur_time->tm_hour;\
		    		tim[1] = cur_time->tm_min;\
				tim[2] = cur_time->tm_sec;\
				TIME_S(tim,format_str,(ptr));\
			} while (0);


