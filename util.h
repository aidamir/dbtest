#include "ramdb.h"
#include <ctype.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <sys/time.h>


typedef struct _au_lotlistitem
{
  lotinfo* lp;
  struct _au_lotlistitem* next;
} lotlistitem;

typedef struct _au_lotlist
{
  lotlistitem* head;
  lotlistitem* tail;
  int items;
} lotlist;

typedef struct _au_logonlistitem
{
  account* lp;
  struct _au_logonlistitem* next;
} logonlistitem;

typedef struct _au_logonlist
{
  logonlistitem* head;
  logonlistitem* tail;
  int items;
} logonlist;

int epoch_from_str(char* val, char typ);
int randnotmore(int interval);
struct timeval* timer( struct timeval* tvn );
int int_compare(const void *pa, const void *pb);
void exit_nicely();
