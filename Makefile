#
# Makefile for example programs
#

#subdir = src/test/examples
#top_builddir = ../../..
#include $(top_builddir)/src/Makefile.global

#override CPPFLAGS := -I$(libpq_srcdir) $(CPPFLAGS)
#LIBS += $(libpq)
PGLIB = /usr/lib/postgresql-8.3

# PROGS= testlibpq0 testlibpq1 testlibpq2 testlibpq3 testlibpq4 testlo
#PROGS = testlibpq testlibpq2 testlibpq3 testlibpq4 testlo
SOURCES = lex.config dbtest syslog util
#sql piece morf morfi amorf
LD_FLEX = -lfl
FLEX_RULES = config
LD_PGSQL = -L$(PGLIB) -lpq
PG_INC = -I/usr/include/postgresql
BIN = dbtest
SHLIB = lib$(BIN)
DLOBJS = lex.config.o syslog.o dbtest.o  util.o
#dbtest.o
#top_builddir = /usr/local/src/postgresql-7.1.3
#include $(top_builddir)/src/Makefile.global


all: clean $(FLEX_RULES) $(SOURCES) link

lib: $(SHLIB)

clean: 
	rm -f *.o
	rm -f dbtest
$(SOURCES): % : %.c 
	@echo Compiling $@.c...
	$(CC) -g $(CFLAGS) $(PG_INC) -c $@.c 
	@echo
$(FLEX_RULES): % : %.l.c
	@echo Generating scanner source lex.$(FLEX_RULES).c....
	flex -8 -P$@ $@.l.c
	@echo Scanners Generated.
	@echo
link:
	@echo Linking binary - $(BIN)...
	$(CC) -o $(BIN) $(DLOBJS) $(LD_FLEX) $(LD_PGSQL)
	@echo $(BIN) successfully maked.
$(SHLIB): 
	@echo Linking shared object -$(SHLIB).so....
	$(CC) -shared --export-dynamic $(DLOBJS) $(LDFLAGS) $(LD_FLEX) -o $(SHLIB).so
	cp *.so /usr/local/lib
	@echo $(SHLIB).so maked!

