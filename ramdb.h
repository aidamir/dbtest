/* ��������� ��� �������� ����*/
#include <libpq-fe.h>
#ifndef RAMDBH
#define RAMDBH


#ifndef LOCALNET
#define LOCALNET "192.168.0.255"
#endif

#ifndef UDPSYNCPORT
#define UDPSYNCPORT 5001;
#endif 

/* #ifndef INIT_IO_BUF_SIZE
#define INIT_IO_BUF_SIZE 15000
#endif */

#define SESSION_DURATION 3600 
/* User session will keep an hour without any user activity*/

#ifndef HI_DEFS
#define HI_DEFS
#define HITEM_LEN 64
#define MAX_HITEMS 20
#define MAX_HILEN MAX_HITEMS * HITEM_LEN
#endif

typedef struct _lotinfo
{
    int lotid;
    float bid;
    int bidcnt;
    int start_epoch; /* ����� ������ � ��������*/
    int stop_epoch; /* ����� ��������� � ��������*/
    int timestamp_epoch; /* ����� ���������� ��������� � ����*/
    char shistory[MAX_HILEN]; /* ������� � ������� ������ ������� h username|bids|bid*/
    char* history; /* ������������ ���� �������� shistory ��������*/
    char username[32];
    int sessid;
    int stor_index;
} lotinfo;

typedef  struct _account { 
	    int sessid;
	    char sesskey[33];
	    char username[33]; 
	    int start_epoch;
	    int stop_epoch;
	    int stor_index;
	} account;

struct bch
{
    char command;
    int items_count;
    int item_size;
};


PGconn* get_stolen_pgconn(void);

#endif
