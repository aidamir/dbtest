int parse_yytext_by_condition(int lwr_req,
				int called_cond,
				int parse_cond);

int parse_string_by_condition(char* scanbuf,
				 int lwr_req, 
				 int called_cond,
				 int parse_cond);


char* ayytextdup(char* str)
{
	    char* buf=malloc(yyleng+2);
	    if ( buf == NULL ) {
		 printf("\nyytextdup: malloc failed");
		 exit(1);
	    }	    
	    memset(buf,'\0',yyleng+2);
	    strcpy(buf,str);
	    return buf;
}


int parse_yytext_by_condition(int lwr_req, int called_cond,int parse_cond)
{
    char* scanbuf=malloc(yyleng+1);
    parse_string_by_condition(scanbuf,lwr_req,called_cond,parse_cond);
}

int parse_string_by_condition(char* scanbuf,
				 int lwr_req, 
				 int called_cond,
				 int parse_cond)
{
	    
	    char* ptr=scanbuf;

	    if (ptr==NULL) {
		printf("\n parse_string_by_condition: Empty string");
		return 0;
	    }
	    cond_called_from=called_cond;
	    
	    main_buffer=YY_CURRENT_BUFFER; // attention! it is temporary.
	    
	    printf("\n STRING TO PARSE: {%s}",yytext); 
	    
	    if (lwr_req) 
		for (ptr;*ptr!='\0';ptr++) tolower(*ptr);
	    
	    text_buffer = yy_scan_string(scanbuf);
	    
	    dontforgetfree=scanbuf;
	    
	    BEGIN(parse_cond);
			printf("\nBEGIN PARSE RULE\n");

	    return 1;
}
