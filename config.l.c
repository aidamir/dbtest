%option yylineno
%option never-interactive
/* %option array */
/*%option stack*/
%option noyywrap
 /* %option debug */
%{
#include <locale.h>
#include <stdarg.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <stdio.h>
#include <string.h>
#include "dbtest.h"
#include <time.h>

//#include "variables.h"
//#define DBG

// #define va_end va_end(ap)
#define MSG_ERROR 1
#define MSG_WARNING 0 

#define BE_ADD_CONN_STRING_OPT { char val[64] = { "\0" }; \
		GET_TAGVAL(val); \
		strncat(BE.connect_string,yytext,((int)(index(yytext,'=') - yytext) + 1)); \
		strcat(BE.connect_string,val); \
		strcat(BE.connect_string," "); }



#define MAKE_BUF_FROM_CHUNK(b) \
    			{   long buf_size = yy_cp - chunk_start - yyleng; \
    			    b = malloc(buf_size+1); \
    			    memset(b,'\0',buf_size); \
    			    strncpy(b,chunk_start,buf_size); \
    			    b[buf_size]='\0'; }

#define GET_CHUNK_LEN (yy_cp - chunk_start - yyleng) 

/*
static char* buf_create(char* ptr, int len,char act)
{
   static int buf_sz; 
   static char* b; 
   if ( buf_sz == 0 ) { }
   b = malloc(buf_sz + 1); 
   memset(b,'\0',buf_sz); 
   strncpy(b,ptr,buf_sz); 
   b[buf_sz]='\0'; 
}
*/

#define ADD_CHUNK_TO_BUF(bf) \
    			{ \
    			    long buf_size = (strlen(bf) + (yy_cp - chunk_start - yyleng)); \
    			    char* tmp = malloc(buf_size+1); \
    			    memset(tmp,'\0',buf_size+1); \
    			    strncpy(tmp,bf,strlen(bf)); \
    			    strncat(tmp,chunk_start,(yy_cp - chunk_start - yyleng)); \
    			    free(bf); \
    			    bf = tmp; \
    			    bf[buf_size]='\0'; }


#define GET_TAGVAL(v) strncpy((v),(index(yytext,'=') + 2) ,(int)( &yytext[yyleng-1] - index(yytext,'=') - 2 ))
#define GET_DTAGVAL(v) strncpy((v),(index(yytext,'=') + 1) ,(int)( &yytext[yyleng-1] - index(yytext,'=') ))

//v_def	v_defs[256] = { '0' };
q_def	q_defs[256] = { '0' };

v_def	**vdefs;
q_def	**qdefs;	

int vdef_cnt = 0;
int qdef_cnt = 0;
int watch  = 0;

int generate_only = 0;

struct backend_t BE = { 0 };

v_def*	vdef_new();
v_def*	vdef_find(char* name);
int	vdef_load_items( v_def* vd );
int	vdef_load_list(v_def* vd,char *list);
char file_search_path[128]={"vals/"};

/*
extern char qbuf[8192] = {""};
extern char numstr[128] = {""};
*/

void msg( int level, const char* fmt, ... )
{
va_list ap;
    switch (level){
    case MSG_WARNING:
    	fprintf(stderr,"config.xml:[%d] WARNING:",yylineno);    
	break;
    case MSG_ERROR:    
    	fprintf(stderr,"config.xml:[%d] Parse ERROR:",yylineno);    
	break;
    default:
	fprintf(stderr,"config.xml:");    
    }
    va_start(ap,fmt);
    	vfprintf(stderr,fmt,ap);
    va_end(ap);
    fprintf(stderr,"\n");
    if ( level == MSG_ERROR ) exit(1);
}

// #define DBG

#ifdef DBG
    void dbg( const char* fmt, ... )
    {
    	va_list ap;
    	printf("\nsrcline:[%d]",yylineno);
    	    va_start(ap,fmt);
    	    	vprintf(fmt,ap);
    	    va_end(ap);
    }
#else
    void dbg( const char* fmt, ... ){};
#endif


%}

ws    [ \t]
sep   [ \t\n]		
nonws [^ \t\n\<\>\.\,\;\)\(\?\!\"\'\&\\]
pt [.,:;)(?!"']
bs ([\\])*([^ \t\n<])+
doctype ("<!DOCTYPE"+([^\>]+)*">"){1}
start_comment "<!--"{1}
end_comment "-->"{1}
right_slash "/"{1}
comment {start_comment}{1}+[^{end_comment}]*{end_comment}{1}
predl "."+{ws}*[A-Z]+
nbsp "&nbsp"

optname [A-Za-z\_]+
option	[A-Za-z\_]+[=]{1}["][A-Za-z\_\/\.]+["]
opt_val ["][A-Za-z0-9\_\/\.]+["]
opt_val_seq ["][A-Za-z0-9\_\/\.\,]+["]
backend_opt_val [A-Za-z0-9\_]+

oper {op_set}|{op_eqaul}
op_set ([ \n\t]*[=]{1}[ \n\t]*){1}
op_equal ([ \n\t]*[=]{2}[ \n\t]*){1}

tag "<"[a-z\_]+">"
etag "</"[a-z\_]+">"
utag "<"[a-zA-Z\_]+">"*
uetag "</"[a-zA-Z\_]+">"*
val_date ["][0-3]*[0-9]{1}[\:\-](("0"[0-9])|"11"|"12"|"10")[\:\-][0-9]{2,4}["]
val_time ["][0-9]{2}[\:\-][0-9]{2}[\:\-][0-9]{2}["]



/* Conditions */

%x backend_body
%x comment
%x seekend
%x value
%x sql_body
%x not_defined
%x eatup_line
%x query

%{
// #define VDEF v_defs[vdef_cnt]
#define VDEF vdefs[vdef_cnt - 1]

#define QDEF q_defs[qdef_cnt]
#define VLEN_LONG 256
#define VLEN_SHORT 64
#define VLEN_MIDDLE 128

%}

%% 
    char* chunk_start;
    char* buf_ptr; 
    char* bf;
    tag cur_tag = tnone;
    int vlist_start = 0;

"<backend>"|"<backend" {
		cur_tag = tbackend;
		BEGIN(backend_body);
	    }
<backend_body>"host="{1}{backend_opt_val} { BE_ADD_CONN_STRING_OPT }
<backend_body>"user="{1}{backend_opt_val} { BE_ADD_CONN_STRING_OPT }
<backend_body>"password="{1}{backend_opt_val} { BE_ADD_CONN_STRING_OPT }
<backend_body>"dbname="{1}{backend_opt_val} { BE_ADD_CONN_STRING_OPT }

<backend_body>"connect_string="{1}["][^\"]+["] { 
		char val[VLEN_LONG]={'\0'} ;
		GET_TAGVAL(val); 
		strncpy(BE.connect_string,val,VLEN_LONG); 
		}
<backend_body>"connections="{1}[0-9]+ {
		char val[VLEN_SHORT] = {'\0'};
		GET_DTAGVAL(val);   
		BE.max_conn = atoi(val);
	    }
<backend_body>"report_interval="{1}[0-9]+ {
		char val[64] = { '\0' };
		GET_DTAGVAL(val); BE.report_interval = atoi(val);
	    }
<backend_body>"reset_stat_interval="{1}[0-9]+ {
		char val[64] = { '\0' };
		GET_DTAGVAL(val); BE.reset_stat_interval = atoi(val);
	    }
<backend_body>"query_limit="{1}[0-9]+ {
		char val[64] = { '\0' };
		GET_DTAGVAL(val); BE.query_limit = atoi(val);
	    }
<backend_body>"query_interval="{1}[0-9]+ {
		char val[64];
		GET_DTAGVAL(val); BE.query_interval = atoi(val);
	    }
<backend_body>"generate_only=Y" {
		generate_only = 1;
	    }
<backend_body>"duration="{val_time} { 
		char val[64]={'\0'} ;
		GET_TAGVAL(val);
		BE.duration = second_from_str(val);
		}
<backend_body>"</backend>"|"/>" {
		BEGIN(INITIAL); 
	    }	
"<value>"|"<value"{1} {
		BEGIN(value);		
		cur_tag = tvalue;
		dbg("[start of value]\n");
	    }
<value,backend_body,query>"/"*">"{1} {
			switch ( cur_tag )
			{
			case tbackend:
			    break;
			case tquery:
			    break;
			case tvalue:
			    if ( strlen( VDEF->name ) == 0 ) {
				msg(MSG_ERROR,"value name must be provided.");
			    } 
			
		    	    if ( yytext[0] == '>' ) {
				BEGIN(eatup_line);
				dbg("[start of value list]");
			    }
			} // end cur_tag case
			    dbg("[end of tag breif]\n");
		    	if ( yytext[0] == '/' ) {
			    cur_tag = tnone;
			    BEGIN(INITIAL);
			}
		    }
<value>[^{right_slash}]">" {
		      if ( strlen( VDEF->name ) == 0 ) {
		        msg(MSG_ERROR,"value name must be provided.");
		      } 
		      BEGIN(eatup_line); 	
/*
		      chunk_start = yy_cp;
		      vlist_start = 1;*/
			dbg("[end of value desc]\n");

		    }
<value,eatup_line>{ws}*"</value>"{1}{ws}* {
/*
		if ( yy_cp - chunk_start - yyleng > 0 )
		{       
			dbg("chunk_start %s",chunk_start);
		
			MAKE_BUF_FROM_CHUNK(bf);
			dbg("list bf[%s]",bf);
			vdef_load_list(&VDEF,bf); 
			free(bf);
		}*/

		BEGIN(INITIAL);
		dbg("[END OF VDEF]\n",yytext);
	    }
<eatup_line>^[^\<\n]+ { 
//		    dbg("[eat_line %s]\n",yytext);
		}
<value>{ws}{1}"type="{opt_val} {
		if ( strcmp(&yytext[6],"\"integer\"") == 0 ) {
			VDEF->vtype = vint;
		} else if ( strcmp(&yytext[6],"\"vdate\"") == 0 ) {
			VDEF->vtype = vdate; 
		} else if ( strcmp(&yytext[6],"\"vtime\"") == 0 ) {
			VDEF->vtype = vtime; 
		} else if ( strcmp(&yytext[6],"\"vtimestamp\"") == 0 ) {
			VDEF->vtype = vtimestamp; 
		} else {
		    msg(MSG_ERROR,"Inavid value type: %s\n",&yytext[6]);
		}
		dbg("Setting value type to %d",VDEF->vtype);	    
	    }
<value,query>{ws}{1}"name="{opt_val} {
		switch (cur_tag) {
		case tquery:
			strncpy(QDEF.name,&yytext[7],yyleng - 8 ); 
		    dbg("[name] %s\n",&yytext[6]);
		    break;
		case tvalue:
			vdef_new();
//			strncpy(vdefs[vdef_cnt1 - 1]->name,&yytext[7],yyleng - 8 ); 
			strncpy(VDEF->name,&yytext[7],yyleng - 8 ); 
			dbg("[name] %s\n",&yytext[6]);
		    break;
		} 
	    }
<value>{ws}{1}"selection="{opt_val} {
		char val[64] = { '\0' };
		GET_TAGVAL(val);
		if ( strcmp(val,"random") == 0 ) VDEF->selection = 'R';
		else if ( strcmp(val,"sequent") == 0 ) VDEF->selection = 'S';
		else {
		    msg(MSG_ERROR,"Selection type of the value can be \"random\"|\"sequent\"." );
		}
	    }
<value>{ws}{1}"min="{val_time} {
		char val[64] = { '\0' };
		GET_TAGVAL(val);

		dbg("Found TIME %s",val);

		if ( ( VDEF->min = second_from_str(val) ) < 0 ) 
		    msg(MSG_ERROR,"Bad time representation [%s]",yytext); 
	    }
<value>{ws}{1}"max="{val_time} {
		char val[64] = { '\0' };
		GET_TAGVAL(val);
		dbg("Found TIME %s",val);

		if ( ( VDEF->max = second_from_str(val) ) < 0 ) 
		    msg(MSG_ERROR,"Bad time representation [%s]",yytext); 
	    }
<value>{ws}{1}"min="{val_date} {
		char val[64] = {'\0'};
		GET_TAGVAL(val);
		dbg("Found Date %s",val);

		if ( ( VDEF->min = epoch_from_str(val,'D') ) < 0   ) 
		    msg(MSG_ERROR,"Bad date representation [%s]",yytext); 
	    }
<value>{ws}{1}"max="{val_date} {
		char val[64] = { '\0' };
		GET_TAGVAL(val);
		dbg("Found Date %s",val);
	
		if ( ( VDEF->max = epoch_from_str(val,'D') ) < 0   ) 
		    msg(MSG_ERROR,"Bad date representation [%s]",yytext); 

	    }
<value>{ws}{1}"fname="{opt_val} {
		char val[128] = { '\0' };
		GET_TAGVAL(val);

		if ( strlen( VDEF->name ) == 0 ) msg(MSG_ERROR,"Tag 'name=' must be defined first.");
		
		VDEF->vtype = vfile;
		
		strcpy(VDEF->fname,val); 

//		vdef_load_items(&VDEF,val);
				
		dbg("[fname] %s...\n",VDEF->fname);
	    }
<value>{ws}{1}"min="{opt_val} {
		char val[64] = { '\0' };;
		GET_TAGVAL(val);   
		
		if ( VDEF->vtype == vint ) {
			VDEF->min = atoi(val);
		} else { 
		    msg(MSG_ERROR,"Unknown value type or it must be defined first.");
		}		
	    }
<value>{ws}{1}"max="{opt_val} {
		char val[64] = { '\0' };
		GET_TAGVAL(val);   

		if ( VDEF->vtype == vint ) {
			VDEF->max = atoi(val);
		} else {
		    msg(MSG_ERROR,"Unknown value type or it must be defined first.");		
		}	
	    }
"<query>"|"<query"{1} {
		cur_tag = tquery;
		bf = NULL;
		BEGIN(query);
		dbg("[start of query]\n");
	    }
<query>{ws}{1}"density="[0-9]+ {
		    char val[64] = {'\0'};
		    GET_DTAGVAL(val);
		    dbg(" Found Density %s" ,val);
		    QDEF.density = atoi(val);
		}
<query>{ws}{1}"measure="[0-9]+ {
		    char val[64] = {'\0'};
		    GET_DTAGVAL(val);
		    dbg(" Found Measure %s" ,val);
		    QDEF.measure = atoi(val) * 1000;
		}    
<query>"</query>"{1} {
		BEGIN(INITIAL);
		dbg("[end of query]\n");
	    }
<value,query>"<sql>"|"<SQL>" {
    			      chunk_start = yy_cp;
			      if  ( qdef_cnt == 58 ) {
			      		watch = 1;
			      }
			      bf = malloc(1);     
	    		      memset(bf,'\0',1); 	
				    dbg("[SQL] {%s},chunk_start = %d,yy_cp = %d, qdef_cnt =%d\n",yytext,chunk_start,yy_cp,qdef_cnt);
		}
<value,query>"<#"[A-Za-z0-9\_]+"#>" {
		    v_def* vd = 0;
		    char vname[64]={'\0'};
		    dbg("chunk_start = %d, yy_cp = %d dif %d \n",chunk_start,yy_cp,yy_cp - chunk_start);
		    dbg("[SQL VALUE] {%s}\n",yytext);
		    strncpy(vname,&yytext[2],yyleng - 4);
			dbg("chunk_start = %d, yy_cp = %d dif %d \n",chunk_start,yy_cp,yy_cp - chunk_start);
		    if ( (vd = vdef_find(vname)) > 0 ) {
			QDEF.voffs[QDEF.v_cnt] = 
					    yy_cp - chunk_start - yyleng;
			QDEF.vdefs[QDEF.v_cnt++] = vd;
			dbg("chunk_start = %d, yy_cp = %d dif %d \n",chunk_start,yy_cp,yy_cp - chunk_start);
			ADD_CHUNK_TO_BUF(bf) 
		    } else { msg(MSG_ERROR,"Value or fragment named: \"%s\""
					"- was not previously defined.",vname); };
		      chunk_start = yy_cp;
		}
<value,query>"</sql>"|"</SQL>"{1} {
			ADD_CHUNK_TO_BUF(bf)
			QDEF.sql = bf;
			dbg("RESULT SQL = {%s}",QDEF.sql);
			bf = NULL;	
			qdef_cnt++;
		}
<value,query,backend_body,INITIAL>{uetag}|{utag}{1} {
			msg(MSG_ERROR,"Unrecognized tag %s",yytext);
		}

<value,query,backend_body,INITIAL>{sep}{1} {}

<value,eatup_line,query,backend_body,INITIAL>(.{1})|(\n{1}) {  
		    if ( bf == NULL )
			msg(MSG_ERROR,"Unrecognized character [%s]",yytext); 
}

<*><<EOF>> {
	    buf_ptr = NULL;
	    dbg("TERMINATE"); 
    	    yyterminate();
    	}
%%

int vdbg = 0;

int main(int argc, char* argvp[])
{
int i;
int n =0 ;    
FILE *F;
/*
q_stat qs;
qs.q_defp = &q_defs[0];
*/

    if ( ( F = fopen("./config.xml","r") ) )
    {
	yyin = F;
	yylex();

	fclose(yyin);
    
/*
// fix it temporary code for    
        for ( i = 0; i < qdef_cnt; i++ ) {
    	    char* temp = strdup(q_defs[i].sql);
//	    free(q_defs[j].sql);
	    q_defs[i].sql = temp;
	}
*/

	for (i = 0; i < vdef_cnt; i++ ) 
	{
	    if ( vdefs[i]->vtype == vfile ){
		vdef_load_items( vdefs[i] );	    
	    }
	}
    
    } else {
	printf("Cannot open ./config.xml .\n");
	exit(0);
    }

    srand(time(NULL)); 

    if ( generate_only ) {
        if ( ( F = fopen("./qout.sql","w+") ) )
	{
    	    int i,j;
	 	for ( j = 0; j < qdef_cnt; j++ ) {
	    		mysrand(j * 10000);
			for ( i = 0; i< BE.query_limit ; i++ ) {  /*  srand(time(NULL)); */ 	fprintf( F,"%s\n",q_clone2(&q_defs[j]) );  	}
		}
	} else printf("Cannot open ./quot.sql .\n");  
	exit(0);
    }

//    srand(time(NULL)); 
//    printf("%s",q_clone(&qs) );

    backend();
}

v_def* vdef_new()
{
    vdefs = realloc(vdefs,(vdef_cnt+1) * sizeof(v_def*));
    vdefs[vdef_cnt] = calloc(1,sizeof(v_def));
    vdefs[vdef_cnt]->vtype=vnone;
    vdefs[vdef_cnt]->selection='S';
    vdefs[vdef_cnt]->fname[0]='\0';
    vdefs[vdef_cnt]->name[0]='\0';
    vdef_cnt++;
    return vdefs[vdef_cnt-1];
}

v_def* vdef_find(char* name){
int i;
    for (i = 0; i < vdef_cnt; i++ ) {
	if ( strcmp(vdefs[i]->name,name) == 0 )
	    return vdefs[i];
    }
    return (v_def*)0;
};

int vdef_load_list(v_def* vd,char* list)
{
    int i;
    char *ptr = list;
    char* vp;
    char** item_ptr = vd->items;

    do {
	*item_ptr = ptr;
	item_ptr++;
	vd->items_count++;
	if ( vd->items_count > MAX_ITEMS )
		msg(MSG_ERROR,"maximum items limit MAX_ITEMS=%d exceed",MAX_ITEMS);
    } while ( strlen(ptr) > 1 && ( ptr = index(ptr,'\n') ) > 0  && ptr++ );

    vd->data = list;
    return i;
}

int vdef_add_item(v_def* vd,char* new_item)
{
#define  VITEM vd->items[vd->items_count]

    if ( strlen(new_item) == 0 ) return 0;
    if ( vd->items_count+1 > MAX_ITEMS ) 
	msg(MSG_ERROR,"maximum items limit MAX_ITEMS exceed");
    VITEM = malloc(strlen(new_item)+1);
    strcpy( VITEM, new_item );
    if ( rindex(VITEM,'\n') > 0 ) *(rindex(VITEM,'\n')) = '\0';
    return vd->items_count++;
}

int vdef_load_items( v_def* vd )
{
FILE* in_file = NULL;
char buf[512]; 
/*struct stat st = { 0 };
stat(vd->fname, &st);*/
int v = 0;
char fname[128] = {'\0'};
    printf(" Loading data from file:%s\n",vd->fname );
    
    if ( strlen(file_search_path) > 0 ) {
	sprintf(fname,"%s%s",file_search_path,vd->fname);
    }
    
    if ( ( in_file = fopen(fname,"r") ) ) {
	while ( fgets(buf,512,in_file) ) {
//	if ( v++ > 42190 ) vdbg = 1;
	// dbg("vdef_item %d\n",vdbg++);
	    vdef_add_item(vd,buf);
	
	}    
    } else {
	msg(MSG_ERROR,"Cannot open file %s\n",vd->fname);
    }
    fclose(in_file);
    return vd->items_count;
}

int epoch_from_str(char* val)
{
struct tm mytm = { 0 };
char tmp[6][5] = { "","","","","","" };
char *ptr = 0; 
int i = 0,j = 0;

    for ( ptr = val; *ptr != '\0' && i < 6 && j < 5; ptr++ ) 
    	if ( isdigit(*ptr) ) tmp[i][j++] = *ptr;
	else { tmp[i++][j]='\0'; j = 0; } 

    mytm.tm_mday = ( strlen(tmp[0]) > 0 ? atoi(tmp[0]) : 0 ); 
    mytm.tm_mon = ( strlen(tmp[1]) > 0 ? atoi(tmp[1]) : 0 ); 
    if ( mytm.tm_mon > 0 ) mytm.tm_mon--;
    mytm.tm_year = ( strlen(tmp[2]) > 0 ? atoi(tmp[2]) : 0 );
    if ( mytm.tm_year > 1900 ) mytm.tm_year -= 1900;
    mytm.tm_hour = ( strlen(tmp[3]) > 0 ? atoi(tmp[3]) : 0 );
    mytm.tm_min = ( strlen(tmp[4]) > 0 ? atoi(tmp[4]) : 0 ); 
    mytm.tm_sec = ( strlen(tmp[5]) > 0 ? atoi(tmp[5]) : 0 ); 

    mytm.tm_isdst = -1;

    return mktime(&mytm);
}

int second_from_str(char* val)
{ 
char tmp[3][3]= { "","","" } ;
char *ptr;
int hour = 0, min = 0,sec = 0;
int i = 0,j = 0;
    if ( strlen(val) > 10 ) return -1; 

    for ( ptr = val; *ptr != '\0' && i < 3 && j < 3; ptr++ ) {
	if ( isdigit(*ptr) ) tmp[i][j++] = *ptr;
	else { 
	    tmp[i++][j]='\0'; j = 0; 
	    }
    } 

    if ( ( ( hour = atoi(tmp[0]) ) > 23 ) || ( min = atoi(tmp[1]) ) > 50 ||
	( sec = atoi(tmp[2]) ) > 59 ) return -1;

    return ( (hour * 3600) + (min * 60) + sec );
}



