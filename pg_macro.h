#include "libpq-fe.h"

#define SQL_REQUEST_CURSOR_PROC \
"DECLARE request CURSOR FOR select rqmultix('se_urq%s',%s,'%s');"
//#define DEBUG

#define SQLBEGIN\
	res = PQexec(conn, "BEGIN");\
	if (PQresultStatus(res) != PGRES_COMMAND_OK)\
	{	printf( "BEGIN command failed\n%s",PQerrorMessage(conn));\
		PQclear(res);\
		exit_nicely(conn);\
	} \
	PQclear(res);

#define SQLEXECCOK(st)\
	res = PQexec(conn, st);\
	if (PQresultStatus(res) != PGRES_COMMAND_OK)\
	{	printf( "sql exec command failed!\n%s",PQerrorMessage(conn));\
		PQclear(res);\
		exit_nicely(conn);\
	} 

#define SQLEXECTOK(st)\
        res = PQexec(conn,st);\
	if (PQresultStatus(res) != PGRES_TUPLES_OK)\
	{	printf("\nNothing can fetch!\n%s",PQerrorMessage(conn));\
		PQclear(res);\
		exit_nicely(conn);\
	}


#define SQLEND	res = PQexec(conn, "END");\
		PQclear(res);
