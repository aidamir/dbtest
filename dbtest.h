#include <libpq-fe.h>
#include <sys/time.h>

#define MAX_ITEMS 1000000

#define BS_LOG_MARK  __FILE__, __LINE__, __FUNCTION__

int bs_log(int level, char * file, int line, char * function, char *fmt, ...);

//#define BS_LOG_LEVEL  5

#ifndef BS_LOG_LEVEL 
    #define BS_LOG_LEVEL 2 
#endif

#define MAX_LOG_MESSAGE 1024*4

typedef enum _tags {tnone,tbackend,tvalue,tquery} tag;
typedef enum _value_type {vnone,vint,vlist,vfile,vsql,vtime,vdate,vtimestamp,vfragment} value_type;


#define SELECT_TYPE_RANDOM 1
#define SELECT_TYPE_SEQUENT 2 

typedef struct value_def{
    char	name[64];
    value_type	vtype;
    char	fname[64];
    char	*sql;	
    char	selection;
    int 	refresh_inerval;
    int		items_count;
    int		pos;
    int		min;
    int		max;
    char	*data;
    char	*items[MAX_ITEMS];
} v_def;

typedef struct query_def{
    char	name[64];
    char	*sql;
    struct timeval	tv_total;
    float	min_time;
    float	max_time;
    long	sent;
    long	sent_stat;
    long    	processed;
    int		density; // send not less than q_limit queries in measure milliseconds
    int 	measure; // millisecs
    int		concurrent_lim; // concurrent limit
    long	last_sent; // the time when a last portion sent millisecs 	
    long	next_send; // time when a next query will sent
    int		reminder; // the reminder of queries haven't been sent to provide q_lim/t_lim load 
    v_def	*vdefs[64];
    int		voffs[64];
    int 	forgotten;
    int		errors;
    int 	v_cnt;	
} q_def;

typedef struct qstat_t{
    q_def*	qdef;
    struct timeval	tv_sent; // if 0 connection is free for using.
    char*	sql;
    PGconn* 	conn;
} q_stat;

struct backend_t{
    char	connect_string[256];
    char	host[256];
    char	user[64];
    char	password[32];
    char	dbname[64];
    int		max_conn;
    int		report_interval;
    int		query_limit;
    int		query_interval;
    int		reset_stat_interval;
    int 	duration;
};

char* q_clone(q_stat* qs); 

char* q_clone2(q_def* qd); 

char* v_clone(v_def* v);

int q_result_handle(int i);

PGconn* get_free_pgconn();

int backend();
